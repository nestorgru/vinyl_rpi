# Vinyl RPi w/ PID reg
## Hardware
* Raspberry Pi B+
* [Archonarm](https://archlinuxarm.org) latest (yaourt -Syyu --aur --noconfirm)
* Unitra G-602
* 12V powersource
* L298D drive
* DC-DC 12V-5V (for rpi)
* 2xhall sensors (one for speed sensor, second for autostop)
* about 2 meter of cabling ;)
* about 2 hours (or days) ;)
## Installation
```shell
yaourt -S python python-pigpio-git # *python is important!*
mv pigpiod.service /etc/systemd/system/
mv Vinyl_RPi.service /etc/systemd/user/
systemctl enable pigpiod
systemctl enable Vinyl_RPi
systemctl start pigpiod
systemctl start Vinyl_RPi
```
## PIN connection
```python
pin_motor_dir = 16 # 17
pin_motor_pwm = 19 # 18
pin_strobe_dir = 17 # 16
pin_strobe_pwm = 18 # 19
pin_hall_speed = 4
pin_hall_tonearm = 12
pin_relay_tonearm = 21
pin_relay_aux = 20  # power
pin_speed_33 = 23
pin_speed_45 = 24
```
## systemd
pigpiod *-g* running in foreground (dont want to make pid files etc), python running with unbuffered IO, *PYTHONUNBUFFERED=x* (this make possible use of 'systemctl --user status Vinyl_RPi' command).
## done:
* autostop
# T.B.D.
* encoder for speed adjustement
